variable "aws_region" {
  description = "Region for the VPC"
  default = "us-west-2"
}

variable "vpc_cidr" {
  description = "CIDR for the VPC"
  default = "10.202.0.0/16"
}

variable "public_subnet_cidr" {
  description = "CIDR for the public subnet"
  default = "10.202.1.0/24"
}

variable "public_subnet2_cidr" {
  description = "CIDR for the public subnet"
  default = "10.202.2.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR for the private subnet"
  default = "10.202.3.0/24"
}

variable "private_subnet2_cidr" {
  description = "CIDR for the private subnet"
  default = "10.202.4.0/24"
}

variable "ami" {
  description = "IIS 10 WINDOWS 2019"
  default = "ami-02140442ccd3fa307" #"ami-09592ff2ba4124e66" #"ami-02140442ccd3fa307"
}

variable "key_path" {
  description = "SSH Public Key path"
  default = "/home/.ssh/aws/public"
}

variable "admin_username" {
  description = "EC2 user name"
  default = "hagay"
}

variable "admin_password" {
  description = "EC2 user password"
  default = "Password"
}

variable "db_instance_class" {
  description = "db.t2.micro"
  default = "db.t2.micro"
}

variable "iis_instance_type" {
  description = "t2.micro"
  default = "t2.micro"
}