<powershell>

####################################################### SET UP WINRM #######################################################

write-output "Running User Data Script"
write-host "(host) Running User Data Script"

Set-ExecutionPolicy Unrestricted -Scope LocalMachine -Force -ErrorAction Ignore

# Don't set this before Set-ExecutionPolicy as it throws an error
$ErrorActionPreference = "stop"

# Remove HTTP listener
Remove-Item -Path WSMan:\Localhost\listener\listener* -Recurse

$Cert = New-SelfSignedCertificate -CertstoreLocation Cert:\LocalMachine\My -DnsName "packer"
New-Item -Path WSMan:\LocalHost\Listener -Transport HTTPS -Address * -CertificateThumbPrint $Cert.Thumbprint -Force

# WinRM
write-output "Setting up WinRM"
write-host "(host) setting up WinRM"

cmd.exe /c winrm quickconfig -q
cmd.exe /c winrm set "winrm/config" '@{MaxTimeoutms="1800000"}'
cmd.exe /c winrm set "winrm/config/winrs" '@{MaxMemoryPerShellMB="1024"}'
cmd.exe /c winrm set "winrm/config/service" '@{AllowUnencrypted="true"}'
cmd.exe /c winrm set "winrm/config/client" '@{AllowUnencrypted="true"}'
cmd.exe /c winrm set "winrm/config/service/auth" '@{Basic="true"}'
cmd.exe /c winrm set "winrm/config/client/auth" '@{Basic="true"}'
cmd.exe /c winrm set "winrm/config/service/auth" '@{CredSSP="true"}'
cmd.exe /c winrm set "winrm/config/listener?Address=*+Transport=HTTPS" "@{Port=`"5986`";Hostname=`"packer`";CertificateThumbprint=`"$($Cert.Thumbprint)`"}"
cmd.exe /c netsh advfirewall firewall set rule group="remote administration" new enable=yes
cmd.exe /c netsh firewall add portopening TCP 5986 "Port 5986"
cmd.exe /c net stop winrm
cmd.exe /c sc config winrm start= auto
cmd.exe /c net start winrm
  
####################################################### SET UP HELLO WORLD WEBSITE #######################################################

echo "<!DOCTYPE html>
<head>
	<title>HTML and CSS Hello World</title>
	<style>
		body {
			background-color: #2D2D2D;
		}
		h1 {
			color: #C26356; /* setting the text color */
			font-size: 60px; /* setting the size of the font */
			font-family: Menlo, Monaco, fixed-width;
		}
		p {
			color: white;
			font-family: Source Code Pro, Menlo, Monaco, fixed-width;
		}
		
	</style>
</head>
<body>
	<h1>Hello World HAGAY</h1>
	<p>This is a very basic Hello World example made up in HTML and CSS for HAGAY EXAMPLE website.</p>
</body>
</html>" > C:\inetpub\wwwroot\iisstart.htm

####################################################### INSTALL 3D PARTY TOOLS #######################################################

# Install Choco
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#Choco install sql-server-express -y --force
choco install dbeaver -y --force

# Create Dbeaver shortcut
$SourceFileLocation = “C:\Program Files\DBeaver\dbeaver.exe”
$ShortcutLocation = “C:\Users\$env:USERNAME\Desktop\dbeaver.lnk”
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
$Shortcut.TargetPath = $SourceFileLocation
$Shortcut.Save()

# user_data logs are at C:\ProgramData\Amazon\EC2-Windows\Launch\Log\UserdataExecution.log

</powershell>
