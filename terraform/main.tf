# Create SSH key pair for our instances
resource "aws_key_pair" "web_ssh_key" {
  key_name = "web_ssh_key"
  public_key = file(var.key_path)

}

# Create AWS as our provider
provider "aws" {
  region                  = var.aws_region
  shared_credentials_file = "/var/lib/jenkins/.aws/credentials"
  profile                 = "default"
}

############################################################### VPC ###############################################################

# Create our VPC
resource "aws_vpc" "main_vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "main_vpc"
  }
}

############################################################# SUBNETS ##############################################################

# Create the public subnet1
resource "aws_subnet" "public-subnet" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = var.public_subnet_cidr
  availability_zone = "us-west-2a"

  tags = {
    Name = "public subnet1"
  }
}

# Create the public subnet2
resource "aws_subnet" "public-subnet2" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = var.public_subnet2_cidr
  availability_zone = "us-west-2b"

  tags = {
    Name = "public subnet2"
  }
}

# Create the private subnet
resource "aws_subnet" "private-subnet" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = var.private_subnet_cidr
  availability_zone = "us-west-2a"

  tags = {
    Name = "private subnet1"
  }
}

# Create the private subnet2
resource "aws_subnet" "private-subnet2" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = var.private_subnet2_cidr
  availability_zone = "us-west-2b"

  tags = {
    Name = "private subnet2"
  }
}

########################################################## INTERNET GATEWAY ##############################################################

# Create the internet gateway
resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = "internet_gateway"
  }
}

############################################################ ROUTE TABLE #################################################################

# Create the route table
resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  tags = {
    Name = "Public Subnet Route Table"
  }
}

# Assign the route table to the public Subnet
resource "aws_route_table_association" "public-rt-ass" {
  subnet_id = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.public-rt.id

}

# Assign the route table to the public Subnet
resource "aws_route_table_association" "public-rt-ass2" {
  subnet_id = aws_subnet.public-subnet2.id
  route_table_id = aws_route_table.public-rt.id

}

############################################################## ELASTIC IP ################################################################

# Create the elastic ip
resource "aws_eip" "elastic_ip" {
  vpc      = true

  tags = {
    Name = "elastic_ip"
  }
}

############################################################ NAT GATEWAY #################################################################

# Create the nat gateway
resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.elastic_ip.id
  subnet_id     = aws_subnet.public-subnet.id
  depends_on = [aws_internet_gateway.internet_gateway]

  tags = {
    Name = "nat_gateway"
  }
}

############################################################ ROUTE TABLE #################################################################

# Create the route table
resource "aws_route_table" "private-rt" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }

  tags = {
    Name = "db-private-rt"
  }
}

# Assign the route table to the private Subnet
resource "aws_route_table_association" "private-rt-ass" {
  subnet_id = aws_subnet.private-subnet.id 
  route_table_id = aws_route_table.private-rt.id 

}

########################################################## SECURITY GROUPS ###############################################################

# Create the security group for public subnet 1
resource "aws_security_group" "sg_public1" {
  name = "sg_public1"
  description = "Allow incoming HTTP connections & SSH access"
  
  ingress {
    from_port = 5985
    to_port = 5986
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

  vpc_id=aws_vpc.main_vpc.id

  tags = {
    Name = "sg_public1"
  }
}

# Create the security group for public subnet 2
resource "aws_security_group" "sg_public2" {
  name = "sg_public2"
  description = "Allow incoming HTTP connections & SSH access"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

  vpc_id=aws_vpc.main_vpc.id

  tags = {
    Name = "sg_public2"
  }
}

# Create the security group for private subnet 1
resource "aws_security_group" "sg_private1"{
  name = "sg_private1"
  description = "Allow traffic from public subnet"

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [var.public_subnet_cidr]
  }

  ingress {
    from_port = 1433
    to_port = 1433
    protocol = "tcp"
    cidr_blocks = [var.public_subnet_cidr]
  }

  ingress {
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = "sg_private1"
  }
}

# Create the security group for private subnet 2
resource "aws_security_group" "sg_private2"{
  name = "sg_private2"
  description = "Allow traffic from public subnet 2"

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [var.public_subnet2_cidr]
  }

  ingress {
    from_port = 1433
    to_port = 1433
    protocol = "tcp"
    cidr_blocks = [var.public_subnet_cidr]
  }

  ingress {
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = "sg_private2"
  }
}

################################################## APPLICATION LOAD BALANCER #####################################################

#Create the application load balancer
resource "aws_lb" "web" {
  name               = "app-load-balancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.sg_public1.id,aws_security_group.sg_public2.id]
  subnets            = [aws_subnet.public-subnet.id,aws_subnet.public-subnet2.id]

  enable_deletion_protection = false

  tags = {
    Environment = "production"
  }
}

# Create the target group
resource "aws_lb_target_group" "web" {
  name     = "web-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main_vpc.id
}

# Create the target group attachment
resource "aws_lb_target_group_attachment" "web" {
  target_group_arn = aws_lb_target_group.web.arn
  target_id        = "${aws_instance.web.id}"
  port             = 80
}

# Create the load balancer listener
resource "aws_lb_listener" "web" {
  load_balancer_arn = aws_lb.web.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.web.arn}"
  }
}

############################################################ RDS ################################################################

# Create the RDS db subnet group
resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "main"

  subnet_ids = [
    aws_subnet.private-subnet.id,
    aws_subnet.private-subnet2.id
  ]

  tags = {
    Name = "My DB subnet group"
  }
}

#Create the RDS db instance
resource "aws_db_instance" "rds" {
  allocated_storage        = 20
  storage_type             = "gp2"
  engine                   = "mysql"
  port                     = "3306"
  engine_version           = "5.7"
  instance_class           = "db.t2.micro"
  db_subnet_group_name     = aws_db_subnet_group.rds_subnet_group.id
  vpc_security_group_ids   = [aws_security_group.sg_private1.id,aws_security_group.sg_private2.id]
  availability_zone        = "us-west-2a"
  name                     = "rds"
  username                 = "rds"
  password                 = "Passw0rd"
  parameter_group_name     = "default.mysql5.7"
  delete_automated_backups = "true"
  deletion_protection      = "false"
  skip_final_snapshot      = "true"

}

########################################################## SERVERS ###############################################################

#Create webserver inside the public subnet
resource "aws_instance" "web" {
   ami  = var.ami
   instance_type = "t2.micro"
   key_name = aws_key_pair.web_ssh_key.id
   subnet_id = aws_subnet.public-subnet.id
   vpc_security_group_ids = [aws_security_group.sg_public1.id]
   associate_public_ip_address = true
   source_dest_check = false
   get_password_data = true

   user_data = "${file("scripts/SetUpWinRM.ps1")}"
   
}

######################################################### IIS CHECK ###############################################################

# resource "null_resource" "cluster" {

#   provisioner "local-exec" {
#     command = "sleep 60"
#     interpreter = ["PowerShell", "-Command"]
#   }

  # provisioner "local-exec" {
  #   command = "start http://${aws_instance.web.public_ip}"
  #   interpreter = ["PowerShell", "-Command"]
  # }

#   provisioner "local-exec" {
#     command = "start http://${aws_lb.web.dns_name}"
#     interpreter = ["PowerShell", "-Command"]
#   }

#   depends_on = [aws_instance.web]
# }

######################################################### OUTPUTS ################################################################

output "rds_endpoint" {
  value = aws_db_instance.rds.address
}
